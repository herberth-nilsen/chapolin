# chapolin

Componente para tratamento das frases recebidas do STT

#Endpoint
```POST
http://{server}/chapolin/transform
```

#Payload
```
{
        "dialog": "PERGUNTACOMOAJUDAR",
        "phrase": "frase que o STT enviou"
}
```

#Response 

```
//Se sucesso e houve transformação - HTTP Code: 200 
{
    "originalPhrase": "vamos testar essa bunny kane com a palavra anne",
    "newPhrase": "vamos testar essa pane pane com a palavra pane"
}

//Se sucesso mas a frase não foi alterada HTTP Code - 204 - No Content

```

