import request from "supertest";
import app from "../src/app";
import initializePhrases from "./mocks/phrases";
import * as dotenv from 'dotenv';

dotenv.config();

beforeEach(async () => {
  await new initializePhrases().insertPhrases();
});

afterAll(async () => {
  await new initializePhrases().clear();
});

describe('Chapolin Controller', () => {
  test('[GET] - Verifica o método principal se está funcionando', (done: any) => {
    request(app).get('/').then((response: any) => {
      expect(response.statusCode).toBe(200);
      expect(response.body.message).toBe("It\'s works!! - Não contavam com a minha astúcia");

      done();
    });
  });

  test('[POST] - Faz uma chamada com palavras erradas para serem substituidas pelas corretas', (done: any) => {
    request(app).post('/chapolin/transform').send({"phrase": "rebote no meu carro que deu anne"}).then((response: any) => {
      expect(response.statusCode).toBe(200);

      expect(response.body.originalPhrase).toBe("rebote no meu carro que deu anne");
      expect(response.body.newPhrase).toBe("reboque no meu carro que deu pane");

      done();
    });
  });

  test('[POST] - Faz uma chamada com palavras corretas que devem ser mantidas', (done: any) => {
    request(app).post('/chapolin/transform').send({"phrase": "reboque no meu carro"}).then((response: any) => {
      expect(response.statusCode).toBe(204);
    });

    done();
  });
});