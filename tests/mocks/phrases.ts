
import * as mongoose from 'mongoose';
import ChapolinSchema from '../../src/models/chapolin.model';

export default class Phrases {
    private phrases = [
        {
            "operacao": "europ",
            "palavra": "agora",
            "dialogos": [],
            "listaPalavras": [
                "água"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "bateria",
            "dialogos": [],
            "listaPalavras": [
                "baterista",
                "mataria",
                "materia"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "bateu",
            "dialogos": [],
            "listaPalavras": [
                "matthew"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "casa",
            "dialogos": [],
            "listaPalavras": [
                "cara"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "chave",
            "dialogos": [],
            "listaPalavras": [
                "chávez",
                "shaver",
                "xavi",
                "xavier"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "email",
            "dialogos": [],
            "listaPalavras": [
                "meio"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "freio",
            "dialogos": [],
            "listaPalavras": [
                "frank"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "furado",
            "dialogos": [],
            "listaPalavras": [
                "curado"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "moça",
            "dialogos": [],
            "listaPalavras": [
                "monstro"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "pane",
            "dialogos": [],
            "listaPalavras": [
                "anne",
                "pang",
                "bunny",
                "é panico",
                "kane",
                "pammi",
                "panisse",
                "plane",
                "sanne"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "parte",
            "dialogos": [],
            "listaPalavras": [
                "parti"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "perfeito",
            "dialogos": [],
            "listaPalavras": [
                "prefeito"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "pneu",
            "dialogos": [],
            "listaPalavras": [
                "neo",
                "new",
                "pinel"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "pode",
            "dialogos": [],
            "listaPalavras": [
                "bode",
                "podem",
                "rode"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "reboque",
            "dialogos": [],
            "listaPalavras": [
                "rebote",
                "reebok"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "sim",
            "dialogos": [],
            "listaPalavras": [
                "tim",
                "rim",
                "nim",
                "lim",
                "kim",
                "jim",
                "him",
                "gim",
                "cim",
                "bim",
                "pim",
                "dim",
                "fim"
            ]
        },
        {
            "operacao": "europ",
            "palavra": "não",
            "dialogos": [],
            "listaPalavras": [
                "rão",
                "jão",
                "gão",
                "fão",
                "cão",
                "bão",
                "ão",
                "pão"
            ]
        }
    ];

    public async insertPhrases() {
        const Chapolin = mongoose.model('chapolin', ChapolinSchema);
        
        let response: Promise<any> = new Promise(
            (resolve, reject) => {
                Chapolin.insertMany(this.phrases, (err: any, response: any) => {
                    if (err) {
                        console.error('Ocorreu um erro ao salvar as palavras: ', err);
                        reject(err);
                    }

                    resolve(response);
                });
            }
        );

        return response;
    }

    public async clear() {
        const Chapolin = mongoose.model('chapolin', ChapolinSchema);
        
        let response: Promise<any> = new Promise(
            (resolve, reject) => {
                Chapolin.deleteMany((err: any, response: any) => {
                    if (err) {
                        console.error('Ocorreu um erro ao limpar as palavras: ', err);
                        reject(err);
                    }

                    resolve(response);
                });
            }
        );

        return response;
    }
}