FROM node:12.4.0-alpine

WORKDIR /server
COPY . /server

RUN npm install

CMD [ "npm", "run", "dev" ]
