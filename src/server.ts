import * as dotenv from 'dotenv';
import * as Cluster from "cluster";
import * as os from "os";
import  { connect } from 'mongoose';

dotenv.config();
import app from "./app";

// Configura clusterização da aplicação
if (Cluster.isMaster) {
    const cpuCount: number = os.cpus().length;
    const WORKERS_LIMIT: number = parseInt(process.env.WORKERS_LIMIT || '0');
    const maxWorkers: number = WORKERS_LIMIT === 0 || WORKERS_LIMIT > cpuCount ? cpuCount : WORKERS_LIMIT;
    //Cria workers de acordo com a quantidade de cores do processador
    for (let i = 0; i < maxWorkers; i++) {
        Cluster.fork();
    }
} else {
    const PORT = process.env.PORT || 9080;
    app.listen(PORT, () => {
        console.log('[SERVER] is running at localhost:' + PORT);
    
        // Conecta com o mongodb
        const userStringPath = <string> process.env.MONGO_USER ? `${process.env.MONGO_USER}:${process.env.MONGO_PASS}@` : ``;
        const url = <string> `mongodb://${userStringPath}${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_BASE}`;

        connect(url, { useNewUrlParser: true, authSource: 'admin' }).then(function(response) {
            console.log('Mongo conectado com sucesso');
        }, function(response) {
            console.log('Falha ao conectar ao banco de dados');
        });
    });
}