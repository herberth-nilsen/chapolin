import { Document } from "mongoose";

export default interface DictionaryEntry extends Document {
    palavra: string,
    listaPalavras: [String]
}