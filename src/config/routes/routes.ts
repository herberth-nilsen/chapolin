import {Request, Response} from "express";
import ChapolinController from "../../controllers/chapolin.controller";
import HttpStatus from 'http-status-codes';

export class Routes {       

    private chapolinControler: ChapolinController = new ChapolinController();

    public routes(app:any): void {
        app.route('/')
        .get((req: Request, res: Response) => {            
            res.status(HttpStatus.OK).send({
                message: "It works!! - Não contavam com a minha astúcia"
            });
        });
        app.post('/chapolin/transform', async (req: Request, res: Response) => {
            try {
                const context = req.body.context;
                const phrase = req.body.phrase;

                // Validações de parametros obrigatórios do serviço
                if (!phrase || phrase.trim() === '') {
                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: "Informe o parâmetro 'phrase' contendo o texto para realizar transformação" 
                    });
                    return;
                }

                if (!context || context.trim() === '') {
                    res.status(HttpStatus.BAD_REQUEST).send({
                        message: "Informe o parâmetro 'context' contendo o contexto para realizar transformação" 
                    });
                    return;
                }

                const newPhraseString = await this.chapolinControler.transformation(context, phrase);

                // 200 se substituiu alguma palavra e 204 se não houve transformação
                let statusCode = newPhraseString.toLocaleLowerCase() === phrase.toLocaleLowerCase() ? HttpStatus.NO_CONTENT : HttpStatus.OK;

                res.status(statusCode).send({
                    message: 'Frase convertida com sucesso',
                    originalPhrase: phrase,
                    newPhrase: newPhraseString
                });

            } catch (error) { // Se houver alguma falha em qualquer ponto, garante que a app não fique fora do ar
                console.log('Erro ao realizar transformação: ', error);
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ 
                    message: "Erro ao realizar transformação", error: error 
                });
            }
        });
    }
}