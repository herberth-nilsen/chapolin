import * as mongoose from 'mongoose';
import ChapolinSchema from '../models/chapolin.model';
import DictionaryEntry from '../config/types/DictionaryEntry';

/**
 * Controller da Marreta Biônica do Chapolin. 
 * Realiza transformação de palavras em frases por correspondência no dicionário de contexto.
 */
export default class ChapolinController {

    /**
     * Realiza transformação de palavras conhecidas no dicionário.
     * 
     * @param context 
     * @param phrase 
     */
    public async transformation(context: String, phrase: String): Promise<String> {
        
        // Quebra a frase em uma lista de palavras para busca em banco
        let splittedPhrase: String[] = phrase.toLocaleLowerCase().split(' ');
        
        // Busca palavras existentes no dicionario de contexto
        let wordsToChange: DictionaryEntry[] = await this.findByListaPalavras(context, splittedPhrase);

        // Substitui as palavras encontradas no dicionário
        let newPhrase: String[] = splittedPhrase.map((word: String) => {
            let newWord: DictionaryEntry | undefined = 
                wordsToChange.find((t: DictionaryEntry) => t.listaPalavras.indexOf(word) != -1);
            return newWord ? newWord.palavra : word;
        });

        // Remonta a frase com as palavras substituídas pelo dicionário de contexto
        return newPhrase.join(' ');
    }

    /**
     * Busca palavras no dicionário com base em uma lista de palavras.
     * 
     * @param context 
     * @param words 
     */
    private async findByListaPalavras(context: String, words: String[]): Promise<DictionaryEntry[]> {
        const Chapolin = mongoose.model<DictionaryEntry>('chapolin', ChapolinSchema);
        return Chapolin.find({ contexto: context.toLocaleUpperCase(), listaPalavras: { $in: words } });
    }
}