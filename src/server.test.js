import req from "supertest";
import route from "./app";
test("[GET] /", async () => {
    const res = await req(route).get("/");
    expect(res.text).toBe("hello world");
});