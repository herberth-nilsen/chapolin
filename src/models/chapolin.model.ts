import {  Schema } from 'mongoose';

const ChapolinSchema = new Schema({
    operacao: {
        type: String,
        required:'Favor informar a operação'
    },
    palavra: {
        type: String,
        required:'Favor informar a palavra que deveria ser dita'
    },
    listaPalavras:[String]

});

export default ChapolinSchema;