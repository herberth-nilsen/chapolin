import express from 'express';
import *  as bodyParser from 'body-parser';
import { Routes } from './config/routes/routes';

class App {

    public app: express.Application;
    public routes: Routes = new Routes();

    constructor() {
        
        if (!process.env.MONGO_HOST) {
            console.error('[SERVER] - VARIÁVEIS DE CONFIGURAÇÃO DE ACESSO AO MONGO NÃO ENCONTRADAS');
            process.exit(1);
        }
        
        this.app = express();
        this.config();
        //this.mongoSetup();
    }

    private config(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
        //Rotas
        this.routes.routes(this.app);
    }
}
export default new App().app;
